const input = document.getElementById('input');
const btn = document.getElementById('btn');
const result = document.getElementById('result');
const total = document.getElementById('total');
let i = 0;
let k = 0;

// addEventlistener - �����, ����������� ������������ ���������� ������� � ������������� ��������.
// ������� �������� ��� �������, ����� - �������, ���������� ��� ������������� �������.
btn.addEventListener('click', (e) => {
    if (input.value === '') return;
    createDeleteElements2(input.value);
    // console.log(input.value);
    input.value = '';
})

// ��� ������� ��������� �� ���������� ������, ����������� ���������� ��������� � ������.
function createDeleteElements(value) {
    i++;
    k++;
    const li = document.createElement('li');
    const btn = document.createElement('button');
    const ebtn = document.createElement('button');
    li.className = 'li';
    li.textContent = value;

    ebtn.className = "editbtn";
    ebtn.textContent = 'edit';
    li.appendChild(ebtn);

    btn.className = 'btn';
    btn.textContent = 'delete';
    li.appendChild(btn);

    // ������� �������� �������� �� ������
    btn.addEventListener('click', (e) => {
        i--;
        k--;
        total.textContent = i;
        actual.textContent = k;
        result.removeChild(li);
    })
    // ����� ������ appendChild() � removeChild() ������������ ���������� � �������� ������ �� ������ ��������������.

    // ������������ ������ active ������������ ������������ ����������� ������ �� ����� �� ��.
    li.addEventListener('click', (e) => {
        if (k > 0) {
            if ('click' != 2) {
                k--;
                actual.textContent = k;
            } else if ('click' % 2 == 0) {
                k++;
                actual.textContent = k;
            } else {
                actual.textContent = i - 1;
            }
        } else {
            actual.textContent = 0;
        }
    })

    total.textContent = i;
    actual.textContent = k;
    result.appendChild(li);
}

// ��� ������ ����������� �������� ��� ������ edit � delete, � ����� ��������� ������ save.
// ���������� ���������� ���� ��� ����� �����, ������������� ������.
function createDeleteElements2(value) {
    i++;
    k++;
    const inputtask = document.createElement('input');
    const task = document.createElement('div');
    const buttons = document.createElement('div');
    const btn = document.createElement('button');
    const ebtn = document.createElement('button');
    task.className = 'task';
    inputtask.className = 'li';
    inputtask.value = value;
    inputtask.setAttribute('readonly', 'readonly');

    ebtn.className = "editbtn";
    ebtn.textContent = 'edit';

    btn.className = 'btn';
    btn.textContent = 'delete';

    // buttons - ��� ����������� ������ edit(save) � delete
    // ��� ��� ����������� ���� ����������� ��������� ������� ����� �������� � css
    buttons.className = 'buttons';
    buttons.appendChild(ebtn);
    buttons.appendChild(btn);
    task.appendChild(inputtask);
    task.appendChild(buttons);
    result.appendChild(task);
    
    // �������� ��������
    btn.addEventListener('click', (e) => {
        i--;
        k--;
        total.textContent = i;
        actual.textContent = k;
        result.removeChild(task);
    })

    // ���� ����� ����������� ����� ������ save � edit, � �� ������� ������/������
    ebtn.addEventListener('click', (e) => {
        if (ebtn.innerText.toLowerCase() == "edit") {
            ebtn.innerText = "save";
            inputtask.removeAttribute("readonly");
            inputtask.focus();
        } else {
            ebtn.innerText = "edit";
            inputtask.setAttribute("readonly", "readonly");
        }
    });

    // ������������ ������ active ������������ ������������ ����������� ������ �� ����� �� ��.
    inputtask.addEventListener('click', (e) => {
        inputtask.classList.toggle('li-active');
        if(k > 0) {
            if ('click' != 2) {
                k--;
                actual.textContent = k;
            } else if ('click' % 2 == 0) {
                k++;
                actual.textContent = k;
            } else {
                actual.textContent = i - 1;
            }
        } else {
            actual.textContent = 0;
        }
    })

    total.textContent = i;
    actual.textContent = k;
    result.appendChild(task);
}
