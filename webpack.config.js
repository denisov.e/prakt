const HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require('path');

module.exports = {
  entry: "./src/app/index.js",
  mode: "development",
  output: {
    filename: "./main.js",
    chunkFilename: "[name].bundle.js"
  },
  plugins: [  // Array of plugins to apply to build chunk
    new HtmlWebpackPlugin({
        template: __dirname + "/src/public/index.html",
        inject: 'body'
    })
],
  devServer: {
    static: {
        directory: path.join(__dirname, 'src'),
        watch: true,

      },
      client: {
        progress: true,
      },
    compress: true,
    port: 3001,
    open:true
  }
 }
